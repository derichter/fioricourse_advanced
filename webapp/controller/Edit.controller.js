sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "./BaseController",
    "sap/ui/model/json/JSONModel",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator",
    "sap/m/MessageToast"
], function (Controller, BaseController, JSONModel, Filter, FilterOperator, MessageToast) {
    "use strict";

    return BaseController.extend("_0300.Fiori_Course.controller.Edit", {

        _formFragments: {},

        onInit: function () {

            var oRouter = this.getRouter();

            oRouter.getRoute("Edit").attachMatched(this._onRouteMatched, this);


        },

        _onRouteMatched: function (oEvent) {
            var oView = this.getView(),
                sId = oEvent.getParameter("arguments").Id,
                sAvatarActivPath = "/sap/opu/odata/sap/Z_OD_FIORI_COURSE_0300_SRV/BetterAvatarSet(Id='" + sId + "',Status='A')/$value",
                sAvatarNewPath = "/sap/opu/odata/sap/Z_OD_FIORI_COURSE_0300_SRV/BetterAvatarSet(Id='" + sId + "',Status='N')/$value";


            this._showFormFragment("display");
            oView.byId("btnEdit").setVisible(true);
            oView.byId("btnSave").setVisible(false);
            oView.byId("btnCancel").setVisible(false);


            this.sID = sId;
            this.sAvatarActivPath = sAvatarActivPath;
            this.sAvatarNewPath = sAvatarNewPath;

            oView.byId("pageEdit").bindElement("/MainSet('" + sId + "')");

            oView.byId("avatarDisplay").setSrc(this.sAvatarActivPath);

            this.oTreeTable = this.byId("TreeTableDisplay");

        },

        _showFormFragment: function (sFragmentName) {

            var oPage = this.byId("pageEdit");

            oPage.removeAllContent();
            oPage.insertContent(this._getFormFragment(sFragmentName));
        },

        _getFormFragment: function (sFragmentName) {

            var oFormFragment = this._formFragments[sFragmentName];

            if (oFormFragment) {
                return oFormFragment;
            }

            oFormFragment = sap.ui.xmlfragment(this.getView().getId(), "_0300.Fiori_Course.view." + sFragmentName, this);

            this._formFragments[sFragmentName] = oFormFragment;

            return this._formFragments[sFragmentName];
        },

        handleEdit: function () {

            this._toggleButtonsAndView(true);
            this.getView().byId("avatarChange").setSrc(this.sAvatarActivPath);

        },

        _toggleButtonsAndView: function (bEdit) {

            var oView = this.getView();

            // Show the appropriate action buttons
            oView.byId("btnEdit").setVisible(!bEdit);
            oView.byId("btnSave").setVisible(bEdit);
            oView.byId("btnCancel").setVisible(bEdit);

            // Set the right form type
            this._showFormFragment(bEdit ? "change" : "display");


            if (!bEdit) {

                this.getView().byId("avatarDisplay").setSrc(this.sAvatarActivPath + "?" + new Date().getTime());
                this.oTreeTable = this.byId("TreeTableDisplay");

            } else {

                this.oTreeTable = this.byId("TreeTableEdit");
            }
        },

        handleEditCancel: function () {

            this.getView().getModel().resetChanges();

            this.getView().byId("combo").setValueState(sap.ui.core.ValueState.None);

            var that = this;

            var sID = this.sID;

            this.getModel().callFunction("/CancelAvatarUpload", {
                method: "GET",
                urlParameters: {
                    Id: sID
                },
                success: function (oData, response) {
                    that._toggleButtonsAndView(false);
                },
                error: function (oError) {

                }
            });
        },

        handleEditSave: function () {

            var sID = this.sID,
                oModel = this.getModel(),
                that = this;

            oModel.submitChanges({

                success: function () {

                    if (that.AvatarUploaded) {

                        that.getModel().callFunction("/SetAvatarActiv", {
                            method: "GET",
                            urlParameters: {
                                Id: sID
                            },
                            success: function (oData, response) {
                                that._toggleButtonsAndView(false);
                                delete that.AvatarUploaded;
                            },
                            error: function (oError) {

                            }
                        });

                    } else {

                        that._toggleButtonsAndView(false);

                    }

                },
                error: function (oError) {

                }
            }

            );

            this._toggleButtonsAndView(false);
        },

        handleClose: function () {
            var sNextLayout = this.getModel("LayoutModel").getProperty("/actionButtonsInfo/midColumn/closeColumn");
            this.getRouter().navTo("App", {
                layout: sNextLayout
            });
        },


        onPressAvatar: function () {

            this._getUploadDialog().open();
        },


        _getUploadDialog: function () {

            const sUrl = "/sap/opu/odata/sap/Z_OD_FIORI_COURSE_0300_SRV/MainSet('" + this.sID + "')/MainToBetterAvatar";

            const oFilterID = new Filter("Id", FilterOperator.EQ, this.sID);
            const oFilterStatus = new Filter("Status", FilterOperator.EQ, "A");
            const oFilters = new Filter([oFilterID, oFilterStatus], true);

            if (!this.oUploadDialog) {

                this.oUploadDialog = sap.ui.xmlfragment("_0300.Fiori_Course.view.upload", this);
                this.getView().addDependent(this.oUploadDialog);

            }

            var oUploadCollection = sap.ui.getCore().byId("UploadCollection");
            oUploadCollection.setUploadUrl(sUrl);
            oUploadCollection.getBinding("items").filter(oFilters);

            return this.oUploadDialog;

        },

        onUploadClose: function () {

            var oUploadCollection = sap.ui.getCore().byId("UploadCollection");
            oUploadCollection.getItems()
                .filter(oItem => oItem._status === "Edit")
                .forEach(oItem => oUploadCollection._handleCancel(null, oItem.getId()));
            this._getUploadDialog().close();

        },

        onBeforeUploadStarts: function (oEvent) {

            var oCustomerHeaderSlug = new sap.m.UploadCollectionParameter({
                name: "slug",
                value: oEvent.getParameter("fileName")
            });

            oEvent.getParameters().addHeaderParameter(oCustomerHeaderSlug);

            var oModel = this.getView().getModel();

            oModel.refreshSecurityToken();

            var oHeaders = oModel.oHeaders;

            var sToken = oHeaders['x-csrf-token'];

            var oCustomerHeaderToken = new sap.m.UploadCollectionParameter({

                name: "x-csrf-token",

                value: sToken

            });

            oEvent.getParameters().addHeaderParameter(oCustomerHeaderToken);

            sap.ui.core.BusyIndicator.show();

        },

        onUploadComplete: function (oEvent) {

            var oFilterID = new Filter("Id", FilterOperator.EQ, this.sID),
                oFilterStatus = new Filter("Status", FilterOperator.EQ, "N"),
                oFilters = new Filter([oFilterID, oFilterStatus], true),
                oUploadCollection = sap.ui.getCore().byId("UploadCollection");

            this.getView().getModel().refresh();

            sap.ui.core.BusyIndicator.hide();

            this.getView().byId("avatarChange").setSrc(this.sAvatarNewPath + "?" + new Date().getTime());

            oUploadCollection.getBinding("items").filter(oFilters);

            this.AvatarUploaded = true;

        },


        onAvatarDeleted: function (oEvent) {

            const sID = this.sID,
                oModel = this.getModel(),
                oView = this.getView(),
                that = this;

            oModel.callFunction("/DeleteAvatar", {
                method: "GET",
                urlParameters: {
                    Id: sID
                },
                success: function (oData, response) {
                    MessageToast.show(that.getResourceBundle().getText("mtAvatarDeleted"));
                    oModel.refresh();
                    oView.byId("avatarChange").setSrc("");
                },
                error: function (oError) {

                }
            });

        },

        onAvatarRenamed: function (oEvent) {

            this.getModel().submitChanges();

        },

        onCollapseAll: function () {

            this.oTreeTable.collapseAll();
        },

        onCollapseSelection: function () {
            this.oTreeTable.collapse(this.oTreeTable.getSelectedIndices());
        },

        onExpandFirstLevel: function () {
            this.oTreeTable.expandToLevel(1);
        },

        onExpandSelection: function () {
            this.oTreeTable.expand(this.oTreeTable.getSelectedIndices());
        },

        onAddEntry: function () {

            var oNodeModel = this.getModel("Nodes"),
                oModelData = oNodeModel.getData();


            oModelData.catalog.clothing.categories.push({name: "New Entry", categories: []});

            oNodeModel.setData(oModelData);

        }

    });

});