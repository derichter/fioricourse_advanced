sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/MessageBox",
	"_0300/Fiori_Course/model/formatter",
	"../localService/mockserver"
], function (BaseController, JSONModel, Filter, FilterOperator, MessageBox, Formatter, Mockserver) {
	"use strict";

	return BaseController.extend("_0300.Fiori_Course.controller.App", {

		formatter: Formatter,

		//#####################################################################################

		onInit: function () {

			this.getOwnerComponent().getModel().setSizeLimit(200);

		},

		//#####################################################################################

		onSearch: function (oEvent) {

			//var sText = this.getView().byId("search").getValue();
			const sQuery = oEvent.getSource().getValue(),
				oFilter = new Filter("Id", FilterOperator.Contains, sQuery);

			this.getView().byId("rtMain").getBinding("items").filter(oFilter);

		},

		//#####################################################################################

		onDetailPress: function (oEvent) {

			const oRouter = sap.ui.core.UIComponent.getRouterFor(this),
				oSource = oEvent.getSource(),
				oBindingContext = oSource.getBindingContext(),
				oBCObject = oBindingContext.getObject(),
                oNextUIState = this.getHelper().getNextUIState(1);

			oRouter.navTo("Edit", {
				Id: oBCObject.Id,
                layout: oNextUIState.layout
			});

		},

		//#####################################################################################

		onMainAdd: function () {

			this._getDialog().open();
		},

		//#####################################################################################

		onMainAddClose: function () {

			this._getDialog().close();
		},

		//##############################################################################################################

		_getDialog: function () {

			if (!this._oDialog) {

				this._oDialog = sap.ui.xmlfragment("_0300.Fiori_Course.view.add", this);
				this.getView().addDependent(this._oDialog);
			}

			return this._oDialog;
		},

		//##############################################################################################################

		_validateAdd: function () {

			var oInputId = sap.ui.getCore().byId("addId"),
				sID = oInputId.getValue();

			if (sID !== "") {

				oInputId.setValueState(sap.ui.core.ValueState.None);
				return true;

			} else {

				oInputId.setValueState(sap.ui.core.ValueState.Error);
				return false;
			}

		},

		//##############################################################################################################

		onMainAddSave: function () {

			if (this._validateAdd()) {

				var oModel = this.getModel(),
					sID = sap.ui.getCore().byId("addId").getValue(),
					iNumber = sap.ui.getCore().byId("addNumber").getValue(),
					oDate = sap.ui.getCore().byId("addDate").getDateValue(),
					sString = sap.ui.getCore().byId("addString").getValue(),
					bBoolen = sap.ui.getCore().byId("addBoolean").getSelected(),
					sStatus = sap.ui.getCore().byId("addStatus").getSelectedKey(),
					oEntry = {},
					that = this;

				oEntry.Id = sID;

				if (iNumber !== "") {
					oEntry.Number = iNumber;
				}

				oEntry.Date = oDate;
				oEntry.String = sString;
				oEntry.Boolean = bBoolen;

				if (sStatus !== "") {
					oEntry.Status = sStatus;
				}

				oModel.create("/MainSet", oEntry, {

					success: function (response) {
						that.onMainAddClose();
					},
					error: function (response) {
						var obj = JSON.parse(response.responseText);
						MessageBox.error(obj.error.message.value);
					}

				});

			}

		},

		//##############################################################################################################

		onMainDelete: function (oEvent) {

			var oSource = oEvent.getSource().getParent(),
				oBindingContext = oSource.getBindingContext(),
				oBCObject = oBindingContext.getObject(),
				that = this;

			MessageBox.warning(this.getResourceBundle().getText("mbDelete", oBCObject.Id), {
				actions: [MessageBox.Action.OK, MessageBox.Action.CANCEL],
				emphasizedAction: MessageBox.Action.OK,
				onClose: function (sAction) {

					if (sAction === "OK") {

						that._deleteMain(oBindingContext.getPath());
					}
				}
			});

		},

		//##############################################################################################################

		_deleteMain: function (sPath) {

			this.getModel().remove(sPath, {
				success: function (response) {
					// success handling
				},
				error: function (response) {
					// error handling
				}
			});
		},

		//##############################################################################################################

		onMainCallFunction: function () {

			this.getModel().callFunction("/testFunction", { // function import name
				method: "GET", // http method
				urlParameters: {
					"String": "value1"
				}, // function import parameters        
				success: function (oData, response) {
					
				}, // callback function for success
				error: function (oError) {
					
				} // callback function for error
			});
		}

	});

});