sap.ui.define([], function () {
	"use strict";

	return {

		statusValue: function (Status) {

			switch (Status) {
			case "1":
				return sap.ui.core.MessageType.Success;
			case "2":
				return sap.ui.core.MessageType.Warning;
			case "3":
				return sap.ui.core.MessageType.Error;
			default:
				return sap.ui.core.MessageType.None;
			}
		}

	};

});