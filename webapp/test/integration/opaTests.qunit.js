/* global QUnit */

QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"_0300/Fiori_Course/test/integration/AllJourneys"
	], function () {
		QUnit.start();
	});
});
