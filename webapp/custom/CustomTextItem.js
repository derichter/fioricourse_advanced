sap.ui.define([
    "sap/ui/core/Item"
], function (
    Item
) {
    "use strict";
    
    return Item.extend("_0300.Fiori_Course.custom.CustomTextItem", {
        metadata: {
            properties : {
                text : {
                    type : "string", 
                    group : "Misc", 
                    defaultValue : ""
                },
                key : {
                    type : "string", 
                    group : "Data", 
                    defaultValue : null
                }
            }
        }
    });
});