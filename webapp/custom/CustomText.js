sap.ui.define([
    "sap/ui/core/Control"
], function (
    Control
) {
    "use strict";
    
    return Control.extend("_0300.Fiori_Course.custom.CustomText", {
        metadata: {
            properties: {
                selectedKey: {
                    type: "string",
                    group: "Data",
                    defaultValue: ""
                }
            },
            defaultAggregation: "items",
            aggregations: {
                items: {
                    type: "sap.ui.core.Item",
                    multiple: true,
                    singularName: "item",
                    bindable: "bindable"
                }
            }
        }
    });
});