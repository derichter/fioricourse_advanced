sap.ui.define([
    "sap/m/UploadCollectionRenderer"
], function (
    UploadCollectionRenderer
) {
    "use strict";

	var CustomUploadCollectionRenderer = UploadCollectionRenderer;

	return CustomUploadCollectionRenderer;
});