sap.ui.define([
	'sap/ui/core/Renderer'
], function(
	Renderer
) {
    "user strict";
	
	var CustomTextRenderer = {
		apiVersion: 2
	};

    CustomTextRenderer.render = function(oRm, oControl) { // <span class="...">...</span>
        oRm.openStart("span"); // begin of container 
        oRm.class("sapMText");
        oRm.openEnd();

        var sSelectedKey = oControl.getSelectedKey();
        var oItem = oControl.getItems().find((oItem) => oItem.getKey() === sSelectedKey);
        if (sSelectedKey && oItem) {
            oRm.text(oItem.getText());
        }
        
        oRm.close("span"); // end of container
    };
    
	return CustomTextRenderer;
});