sap.ui.define([
    "sap/m/UploadCollectionItem"
], function (
    UploadCollectionItem
) {
    "use strict";
    
    return UploadCollectionItem.extend("_0300.Fiori_Course.custom.CustomUploadCollectionItem", {
        metadata: {
            properties: {
                fileStatus: {
                    type: "string",
                    group: "Data",
                    defaultValue: ""
                }
            }
        }
    });
});