sap.ui.define([
    "sap/m/UploadCollection",
	"sap/ui/core/ResizeHandler",
	"sap/ui/Device",
    "sap/m/ComboBox",
    "_0300/Fiori_Course/custom/CustomText",
    "_0300/Fiori_Course/custom/CustomTextItem"
], function (
    UploadCollection,
    ResizeHandler,
    Device,
    ComboBox,
    CustomText,
    CustomTextItem
) {
    "use strict";
    
    return UploadCollection.extend("_0300.Fiori_Course.custom.CustomUploadCollection", {
        metadata: {
            aggregations: {
                types: {
                    type: "sap.ui.core.Item",
                    multiple: true,
                    singularName: "type",
                    bindable: "bindable"
                }
            }
        },

        _getEditableTypeControls: function(oItem) {
            if(!oItem._aEditableControls) {
                oItem._aEditableControls = [];

                // create a combobox with the exact same binding as specified for the aggregation "types":
                // - if the property is binded, binding info exists and can be used to create an identical binding
                //   -> binding needs to be removed from binding info, in order to create a new binding
                // - if no binding exists we just copy the property
                //   -> aggregations must be cloned, otherwise they can get deleted by other controls
                var oControl = new ComboBox({
                    selectedKey: oItem.getBindingInfo("fileStatus") ? (({ binding, ...o }) => o)(oItem.getBindingInfo("fileStatus")) : oItem.getFileStatus(),
                    items: this.getBindingInfo("types") ? (({ binding, ...o }) => o)(this.getBindingInfo("types")) : this.getTypes().map(o => o.clone())
                });
                oItem.addDependent(oControl);
                oItem._aEditableControls.push(oControl);
            }

            return oItem._aEditableControls;
        },

        _getTypeControls: function(oItem) {
            if(!oItem._aControls) {
                oItem._aControls = [];

                // same as _getEditableTypeControls but with our custom text control
                var oControl = new CustomText({
                    selectedKey: oItem.getBindingInfo("fileStatus") ? (({ binding, ...o }) => o)(oItem.getBindingInfo("fileStatus")) : oItem.getFileStatus(),
                    items: this.getBindingInfo("types") ? (({ binding, ...o }) => o)(this.getBindingInfo("types")) : this.getTypes().map(o => o.clone())
                    // items: this.getBindingInfo("types") ? (({ binding, template, ...o }) => Object.assign({ 
                    //     template: new CustomTextItem({
                    //         key: template.getBindingInfo("key") ? template.getBindingInfo("key") : template.getKey(),
                    //         text: template.getBindingInfo("text") ? template.getBindingInfo("text") : template.getText()
                    //     }) 
                    // }, o))(this.getBindingInfo("types")) : this.getTypes()
                });
                oItem.addDependent(oControl);
                oItem._aControls.push(oControl);
            }

            return oItem._aControls;
        },

        _renderContent: function(item, containerId) {
            var sItemId,
                i,
                iAttrCounter,
                iStatusesCounter,
                iMarkersCounter,
                sPercentUploaded,
                aAttributes,
                aStatuses,
                oRm,
                sStatus,
                aMarkers;
    
            sPercentUploaded = item._percentUploaded;
            aAttributes = item.getAllAttributes();
            aStatuses = item.getStatuses();
            aMarkers = item.getMarkers();
            sItemId = item.getId();
            iAttrCounter = aAttributes.length;
            iStatusesCounter = aStatuses.length;
            iMarkersCounter = aMarkers.length;
            sStatus = item._status;
    
            oRm = this._RenderManager;
            oRm.openStart("div"); // text container for fileName, attributes and statuses
            oRm.class("sapMUCTextContainer");
            if (sStatus === "Edit") {
                oRm.class("sapMUCEditMode");
            }
            oRm.openEnd();
            oRm.renderControl(this._getFileNameControl(item));
            // if status is uploading only the progress label is displayed under the Filename
            if (sStatus === UploadCollection._uploadingStatus) {
                oRm.renderControl(this._createProgressLabel(item, sPercentUploaded));
            } else {
                if (iMarkersCounter > 0) {
                    oRm.openStart("div"); // begin of markers container
                    oRm.class("sapMUCObjectMarkerContainer");
                    oRm.openEnd();
                    for (i = 0; i < iMarkersCounter; i++) {
                        oRm.renderControl(aMarkers[i].addStyleClass("sapMUCObjectMarker"));
                    }
                    oRm.close("div");// end of markers container
                }
                if (iAttrCounter > 0) {
                    oRm.openStart("div"); // begin of attributes container
                    oRm.class("sapMUCAttrContainer");
                    oRm.attr("tabindex", "-1");
                    oRm.openEnd();
                    for (i = 0; i < iAttrCounter; i++) {
                        aAttributes[i].addStyleClass("sapMUCAttr");
                        oRm.renderControl(aAttributes[i]);
                        if ((i + 1) < iAttrCounter) {
                            oRm.openStart("div");  // separator between attributes
                            oRm.class("sapMUCSeparator");
                            oRm.openEnd();
                            oRm.unsafeHtml("&nbsp&#x00B7&#160");
                            oRm.close("div");
                        }
                    }
                    oRm.close("div"); // end of attributes container
                }
                if (iStatusesCounter > 0) {
                    oRm.openStart("div"); // begin of statuses container
                    oRm.class("sapMUCStatusContainer");
                    oRm.attr("tabindex", "-1");
                    oRm.openEnd();
                    for (i = 0; i < iStatusesCounter; i++) {
                        aStatuses[i].detachBrowserEvent("hover");
                        aStatuses[i].setTooltip(aStatuses[i].getTitle() +  ":" + aStatuses[i].getText());
                        oRm.renderControl(aStatuses[i]);
    
                        for (var j = i + 1; j < iStatusesCounter; j++) {
                            if (!aStatuses[i].getVisible()) {
                                break;
                            } else if (aStatuses[j] && aStatuses[j].getVisible()) {
                                oRm.openStart("div"); // separator between statuses
                                oRm.class("sapMUCSeparator");
                                oRm.openEnd();
                                oRm.unsafeHtml("&nbsp&#x00B7&#160");
                                oRm.close("div");
                                break;
                            }
                        }
                    }
                    oRm.close("div"); // end of statuses container
                }
            }
            
            ///////////////////////////////////////////// <div class="bsp"></div>
            // render the corresponding controls under the attribut and status container
            oRm.openStart("div"); // begin of container 
            // oRm.class("sapMUCStatusContainer");
            oRm.openEnd();
            var aControls = [];
            if (sStatus === "Edit"){ 
                aControls = this._getEditableTypeControls(item);
            } else {
                aControls = this._getTypeControls(item);
            }
            for (var i = 0; i < aControls.length; i++) {
                var oControl = aControls[i];

                oRm.renderControl(oControl);
            }
            oRm.close("div"); // end of container
            /////////////////////////////////////////////
            
            oRm.close("div"); // end of container for Filename, attributes and statuses
            this._renderButtons(oRm, item, sStatus, sItemId);
            oRm.flush(jQuery(document.getElementById(containerId))[0], true); // after removal to UploadCollectionItemRenderer delete this line
            this._truncateFileName(item);
            this._sReziseHandlerId = ResizeHandler.register(this, this._onResize.bind(this));
            Device.orientation.attachHandler(this._onResize, this);
        },

        _handleClick: function(event, itemId) {
            // If the target of the click event is an editButton, then this case has already been processed
            // in the _handleEdit (in particular, by executing the _handleOk function).
            // Therefore, only the remaining cases of click event targets are handled.
            var $Button = jQuery(event.target).closest("button");
            var sId = "";
            if ($Button.length) {
                sId = $Button.prop("id");
            }
            if (sId.lastIndexOf("editButton") === -1) {
                if (sId.lastIndexOf("cancelButton") !== -1) {
                    this._handleCancel(event, itemId);
                } else if (event.target.id.lastIndexOf("ia_imageHL") < 0 && event.target.id.lastIndexOf("ia_iconHL") < 0 &&
                    event.target.id.lastIndexOf("deleteButton") < 0 && event.target.id.lastIndexOf("ta_editFileName-inner") < 0) {
                    if (event.target.id.lastIndexOf("cli") > 0) {
                        this.sFocusId = event.target.id;
                    }

                    //////////////////////////////////////////////
                    // only execute this if the button was pressed (else it will cancel the edit mode, everytime we try to use the combobox)
                    if (sId.lastIndexOf("okButton") !== -1) {
                        this._handleOk(event, itemId, true);   
                    }
                    //////////////////////////////////////////////

                }
            }
        }
    });
});